import { Bericht } from './bericht';

export class User {

    public id: number;
    public username: string;
    public password: string;
    public role: string;
    public firstname: string;
    public lastname: string;
    public berichtenList: Bericht[];
}
