import { UserComponent } from 'src/app/user/user.component';
import { User } from './user';

export class Bericht {
    public id: number;
    public text: string;
    public title: string;
    public date: Date;
    public userId: number;
}
