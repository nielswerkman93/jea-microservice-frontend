import { Component, OnInit, Inject } from '@angular/core';
import { BerichtService } from 'src/services/bericht.service';
import { MatDialogRef } from '@angular/material';
import { Bericht } from 'src/models/bericht';

@Component({
  selector: 'app-create-bericht-dialog',
  templateUrl: './create-bericht-dialog.component.html',
  styleUrls: ['./create-bericht-dialog.component.scss']
})
export class CreateBerichtDialogComponent implements OnInit {

  private bericht: Bericht = new Bericht();

  constructor(
    private berichtService: BerichtService,
    public dialogRef: MatDialogRef<CreateBerichtDialogComponent>,
  ) {}

  ngOnInit() {
  }

  onsubmit() {
    this.dialogRef.close(this.bericht);
  }
}
