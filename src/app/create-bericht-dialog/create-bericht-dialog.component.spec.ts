import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBerichtDialogComponent } from './create-bericht-dialog.component';

describe('CreateBerichtDialogComponent', () => {
  let component: CreateBerichtDialogComponent;
  let fixture: ComponentFixture<CreateBerichtDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBerichtDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBerichtDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
