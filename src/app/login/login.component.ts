import { Component, OnInit } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from '../oAuth';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from 'src/services/user.service';
import { User } from 'src/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private oauthService: OAuthService, private http: HttpClient, private router: Router, private userService: UserService) {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  ngOnInit() {
    setTimeout(() => {
      const accesToken = sessionStorage.getItem('access_token');
      const key = 'email';
      let user: User = null;
      if (accesToken != null) {
        console.log('post acces_token');
        this.http.post('http://localhost:7070/jeaApp/api/auth/google', accesToken, {
          headers: new HttpHeaders().set('Content-Type', 'text/plain'),
          observe: 'response',
          responseType: 'text'
        }).subscribe(resp => {
          // sessionStorage.removeItem('id_token');
          sessionStorage.setItem('jwt', resp.body.toString());
          sessionStorage.setItem('userEmail', this.oauthService.getIdentityClaims()[key]);
          this.userService.getByEmail(sessionStorage.getItem('userEmail')).subscribe(res => {
            user = res;
            sessionStorage.setItem('userId', user.id.toString());
            });
          // TODO verify JWT on backend redirect to other page.
          this.router.navigate(['/dashboard']);
        });
      }
    }, 1000);

  }

  onLoginClick() {
    this.oauthService.initImplicitFlow();
  }

}
