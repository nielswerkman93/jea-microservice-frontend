import { Component, OnInit } from '@angular/core';
import { BerichtService } from 'src/services/bericht.service';
import { Bericht } from 'src/models/bericht';
import { MatDialog } from '@angular/material';
import { CreateBerichtDialogComponent } from '../create-bericht-dialog/create-bericht-dialog.component';
import { BerichtenWebsocket } from 'src/services/bericht.websocket.service';
import { User } from 'src/models/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private berichten: Bericht[] = [];

  constructor(private berichtService: BerichtService, private matdialog: MatDialog, private berichtenWebsocket: BerichtenWebsocket) {
    this.berichtenWebsocket.getMessages().subscribe(bericht => this.berichten.push(bericht));
  }

  ngOnInit() {
    this.berichtService.findAll().subscribe(res => {
      this.berichten = res;
      console.log(res);
    });
  }

  openCreateBericht() {
    const dialogref = this.matdialog.open(CreateBerichtDialogComponent, { width: '400px' });

    dialogref.afterClosed().subscribe((data: any) => {
      this.createBericht(data.title, data.text);
    });
  }

  createBericht(title: string, text: string) {
    let bericht: Bericht = new Bericht();

    bericht.title = title;
    bericht.text = text;
    bericht.userId = parseInt(sessionStorage.getItem('userId'), null);

    console.log(bericht);
    this.berichtService.create(bericht).subscribe(res => {
      console.log(res);
    });
    this.berichtenWebsocket.sendMessage(bericht);
    bericht = new Bericht();
  }

}
