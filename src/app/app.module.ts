import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';

import { OAuthModule } from 'angular-oauth2-oidc';

import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatTableModule,
  MatToolbarModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatExpansionModule
} from '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BerichtService } from 'src/services/bericht.service';
import { CreateBerichtDialogComponent } from './create-bericht-dialog/create-bericht-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from 'src/services/user.service';
import { BerichtenWebsocket } from 'src/services/bericht.websocket.service';
import { WebSocketService } from 'src/services/websocket.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    HeaderComponent,
    DashboardComponent,
    CreateBerichtDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    //
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatExpansionModule,
    OAuthModule.forRoot()
  ],
  entryComponents: [CreateBerichtDialogComponent],
  providers: [BerichtService,
    UserService,
  BerichtenWebsocket,
WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
