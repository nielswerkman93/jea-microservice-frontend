import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  issuer: 'https://accounts.google.com',
  redirectUri: window.location.origin,
  clientId: '322718159625-cjind2kdc1i5v3edbba5l6na8020dd95.apps.googleusercontent.com',
  scope: 'openid profile email',
  strictDiscoveryDocumentValidation: false
};
