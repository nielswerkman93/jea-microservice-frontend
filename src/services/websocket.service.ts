import { Injectable } from '@angular/core';
import { Subject, Observer, Observable } from 'rxjs';

@Injectable()
export class WebSocketService {
  constructor() {}

  private subject: Subject<MessageEvent>;

  public connect(url): Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
      console.log('Successfully connected the WebSocketService to url: ' + url);
    }
    return this.subject;
  }

  private create(url): Subject<MessageEvent> {
    const websocket = new WebSocket(url);

    const observable = Observable.create((obs: Observer<MessageEvent>) => {
      websocket.onmessage = obs.next.bind(obs);
      websocket.onerror = obs.error.bind(obs);
      websocket.onclose = obs.complete.bind(obs);
      return websocket.close.bind(websocket);
    });
    const observer = {
      next: (data: Object) => {
        if (websocket.readyState === WebSocket.OPEN) {
          websocket.send(JSON.stringify(data));
        }
      }
    };
    return Subject.create(observer, observable);
  }
}
