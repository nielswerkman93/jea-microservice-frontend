import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bericht } from 'src/models/bericht';

@Injectable()
export class BerichtService {
    private readonly baseUrl = 'http://192.168.178.87:9200/api/bericht';

    constructor( private http: HttpClient) {}

    public findAll(): Observable<Bericht[]> {
        return this.http.get<Bericht[]>(this.baseUrl);
      }

      public save(bericht: Bericht): Observable<Bericht> {
        return this.http.put<Bericht>(this.baseUrl, bericht);
      }

      public get(id: number): Observable<Bericht> {
          return this.http.get<Bericht>(this.baseUrl);
      }

      public create(bericht: Bericht): Observable<Bericht> {
          return this.http.post<Bericht>(this.baseUrl, bericht);
      }

      public delete(berichtId: number): Observable<object> {
        return this.http.delete(this.baseUrl + '/' + berichtId);
      }
}
