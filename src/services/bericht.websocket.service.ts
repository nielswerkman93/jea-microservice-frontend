import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { WebSocketService } from './websocket.service';
import { map } from 'rxjs/operators';
import { Bericht } from 'src/models/bericht';

export interface Bericht {
    author: string;
    content: string;
}

@Injectable()
export class BerichtenWebsocket {
    private bericht: Bericht;
    private berichten: Subject<Bericht>;

    constructor(
        private websocketService: WebSocketService,

    ) {
        this.bericht = new Bericht();
        if (sessionStorage.getItem('userEmail').length > 0) {
            const chatUrl =
                'ws://192.168.178.87:9200/chat/' +
                sessionStorage.getItem('userEmail');

            this.berichten = websocketService.connect(chatUrl).pipe(
                map(
                    (response: MessageEvent): Bericht => {
                        const data = JSON.parse(response.data);
                        return {
                            id: data.id,
                            text: data.text,
                            title: data.title,
                            date: data.date,
                            userId: data.userId
                        };
                    }
                )
            ) as Subject<Bericht>;
        } else {
            console.log('User is not logged in, unable to connect');
        }
    }

    public sendMessage(bericht: Bericht) {
        this.berichten.next(bericht);
    }

    public getMessages(): Subject<Bericht> {
        return this.berichten;
    }
}
