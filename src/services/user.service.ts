import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/models/user';

@Injectable()
export class UserService {
    private readonly baseUrl = 'http://192.168.178.87:9100/api/user';

    constructor(private http: HttpClient) { }

    public findAll(): Observable<User[]> {
        return this.http.get<User[]>(this.baseUrl, {
            headers: this.getHeaders(),
        });
    }

    public save(user: User): Observable<User> {
        return this.http.put<User>(this.baseUrl, user);
    }

    public get(id: number): Observable<User> {
        return this.http.get<User>(this.baseUrl + '/' + id);
    }

    public create(bericht: User): Observable<User> {
        return this.http.post<User>(this.baseUrl, bericht, { headers: this.getHeaders() });
    }

    public delete(userId: number): Observable<object> {
        return this.http.delete(this.baseUrl + '/' + userId);
    }

    public getByEmail(email: string): Observable<User> {
        return this.http.get<User>(this.baseUrl + '/' + email, {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('jwt'))
        });
    }


    private getHeaders(): HttpHeaders {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Authorization', 'Bearer ' + sessionStorage.getItem('jwt'));
        return headers;
    }
}
